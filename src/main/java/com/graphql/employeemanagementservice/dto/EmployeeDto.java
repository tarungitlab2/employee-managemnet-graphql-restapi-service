package com.graphql.employeemanagementservice.dto;

public class EmployeeDto {

    private int eId;
    private String eFirstName;

    private String eLastName;

    private String email;

    private Long phone;

    public EmployeeDto() {
    }

    public EmployeeDto(int eId, String eFirstName, String eLastName, String email, Long phone) {
        this.eId = eId;
        this.eFirstName = eFirstName;
        this.eLastName = eLastName;
        this.email = email;
        this.phone = phone;
    }

    public int geteId() {
        return eId;
    }

    public void seteId(int eId) {
        this.eId = eId;
    }

    public String geteFirstName() {
        return eFirstName;
    }

    public void seteFirstName(String eFirstName) {
        this.eFirstName = eFirstName;
    }

    public String geteLastName() {
        return eLastName;
    }

    public void seteLastName(String eLastName) {
        this.eLastName = eLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "EmployeeDto{" +
                "eId=" + eId +
                ", eFirstName='" + eFirstName + '\'' +
                ", eLastName='" + eLastName + '\'' +
                ", email='" + email + '\'' +
                ", phone=" + phone +
                '}';
    }
}
