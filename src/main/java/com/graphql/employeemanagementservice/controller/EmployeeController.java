package com.graphql.employeemanagementservice.controller;

import com.graphql.employeemanagementservice.dto.EmployeeDto;
import com.graphql.employeemanagementservice.exception.ResourceNotFoundException;
import com.graphql.employeemanagementservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;


@Controller
public class EmployeeController {


    private EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }


    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @MutationMapping("createEmployee")
    public EmployeeDto saveEmployee(@Argument EmployeeDto dto)
    {
        return service.save(dto);
    }

    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @QueryMapping("getById")
    public EmployeeDto getByIdEmployee(@Argument int id) throws ResourceNotFoundException {
        return service.getById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @QueryMapping("getEmployees")
    public List<EmployeeDto> getAll(){
        return service.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public EmployeeDto update(EmployeeDto dto,int id) throws ResourceNotFoundException {
        return service.update(dto,id);
    }


    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @MutationMapping("deleteEmployee")
    public String delete(@Argument int id)throws ResourceNotFoundException{
        service.deleteId(id);
        return "Record Has been Deleted Successfully with id : "+id;
    }


}
