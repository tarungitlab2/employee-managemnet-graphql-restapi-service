package com.graphql.employeemanagementservice.service.impl;

import com.graphql.employeemanagementservice.dto.EmployeeDto;
import com.graphql.employeemanagementservice.entity.Employee;
import com.graphql.employeemanagementservice.exception.ResourceNotFoundException;
import com.graphql.employeemanagementservice.repository.EmployeeRepository;
import com.graphql.employeemanagementservice.service.EmployeeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class EmpleeServiceImpl implements EmployeeService {


    private EmployeeRepository repo;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    public EmpleeServiceImpl(EmployeeRepository repo) {
        this.repo = repo;
    }

    @Override
    public EmployeeDto save(EmployeeDto dto) {
        Employee emp = mapper.map(dto,Employee.class);
        emp = repo.save(emp);
        return mapper.map(emp,EmployeeDto.class);
    }

    @Override
    public EmployeeDto getById(int id) throws ResourceNotFoundException {
        Employee emp = repo.findById(id).orElseThrow(()->new ResourceNotFoundException("No Record Found with this Id : "+id));
        return mapper.map(emp,EmployeeDto.class);
    }

    @Override
    public EmployeeDto update(EmployeeDto dto, int id) throws ResourceNotFoundException {
        Employee emp = repo.findById(id).orElseThrow(()->new ResourceNotFoundException("No Record Found with this Id : "+id));
        emp.seteFirstName(dto.geteFirstName());
        emp.seteLastName(dto.geteLastName());
        emp.setEmail(dto.getEmail());
        emp.setPhone(dto.getPhone());
        Employee updateEmp = repo.save(emp);
        return mapper.map(updateEmp,EmployeeDto.class);
    }

    @Override
    public List<EmployeeDto> getAll() {
        List<Employee> empLi = repo.findAll();
        List<EmployeeDto> list =  empLi.stream().map((emp)->mapper.map(emp,EmployeeDto.class)).collect(Collectors.toList());
        return list;
    }

    @Override
    public void deleteId(int id) throws ResourceNotFoundException {
        Employee emp = repo.findById(id).orElseThrow(()->new ResourceNotFoundException("No Record Found with this Id : "+id));
        repo.deleteById(id);

    }
}
