package com.graphql.employeemanagementservice.service;

import com.graphql.employeemanagementservice.dto.EmployeeDto;
import com.graphql.employeemanagementservice.exception.ResourceNotFoundException;

import java.util.List;

public interface EmployeeService {

    public EmployeeDto save(EmployeeDto dto);


    public EmployeeDto getById(int id) throws ResourceNotFoundException;

    public EmployeeDto update(EmployeeDto dto,int id) throws ResourceNotFoundException;

    public List<EmployeeDto> getAll();

    public void deleteId(int id) throws ResourceNotFoundException;

}
